//
//  TPKeyboardAvoidingCollectionView.h
//  FamilyTree
//
//  Created by Megha on 2/18/17.
//  Copyright © 2017 consagous. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"

@interface TPKeyboardAvoidingCollectionView : UICollectionView <UITextFieldDelegate, UITextViewDelegate>
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;
@end
