//
//  MenuVC.swift
//  Hoop
//
//  Created by Anil Makvana on 11/5/16.
//  Copyright © 2016 MacbookPro. All rights reserved.
//

import UIKit
import Firebase


class MenuVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForWalletAmount: UILabel!
    
    
    let picker = UIImagePickerController()
    
    var arrForMenu: NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict1: NSMutableDictionary = ["Name": "Book Your Ride", "image": UIImage(named: "home location")! as UIImage]
        let dict2: NSMutableDictionary = ["Name": "Your Rides", "image": UIImage(named: "cont")! as UIImage]
        let dict3: NSMutableDictionary = ["Name": "Add Money", "image": UIImage(named: "cont")! as UIImage]
        let dict4: NSMutableDictionary = ["Name": "Log Out", "image": UIImage(named: "logout")! as UIImage]
        arrForMenu = [dict1,dict2,dict3,dict4]
        
        tableView.reloadData()
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let userID = UserDefaults.standard.string(forKey: "LOGINUSERVALUE")
        FIRDatabase.database().reference().child("Users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            // Get user value
            let dataDict = snapshot.value as! NSDictionary
            
            self.lblForWalletAmount.isHidden = false
            self.lblForName.isHidden = false
            
            self.lblForName.text = dataDict.value(forKey: "userName") as? String
            self.lblForWalletAmount.text = dataDict.value(forKey: "walletAmount") as? String
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForMenu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.lblForName?.text = ((arrForMenu.object(at: indexPath.row) as AnyObject).value(forKey: "Name") as! String)
        cell.imgForName?.image = ((arrForMenu.object(at: indexPath.row) as AnyObject).value(forKey: "image") as! UIImage)
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
    
        self.findHamburguerViewController()?.hideMenuViewControllerWithCompletion(nil)
        let myString = String(indexPath.row)

        let notificationName = Notification.Name("MenuSelected")
        // Post notification
        NotificationCenter.default.post(name: notificationName, object: myString)
    }
  
    // MARK: - Navigation
    
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewController(withIdentifier: "DLDemoNavigationViewController") as! DLHamburguerNavigationController
    }

}
