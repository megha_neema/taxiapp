//
//  MenuCell.swift
//  Hoop
//
//  Created by MacbookPro on 08/11/16.
//  Copyright © 2016 MacbookPro. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var lblForName: UILabel?
    @IBOutlet weak var imgForName: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
