//
//  CustomNaviVC.swift
//  Hoop
//
//  Created by Anil Makvana on 11/7/16.
//  Copyright © 2016 MacbookPro. All rights reserved.
//

import UIKit

class CustomNaviVC: DLHamburguerViewController {

    var arrForMyCards = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //self.navigationController?.navigationItem.hidesBackButton = true
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func awakeFromNib() {
        self.contentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DLDemoNavigationViewController")
        self.menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC")
    }
}
