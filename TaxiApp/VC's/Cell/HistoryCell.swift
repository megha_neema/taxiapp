//
//  HistoryCell.swift
//  TaxiApp
//
//  Created by Megha on 5/12/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var pickOffLbl: UILabel!
    @IBOutlet weak var rideStatusLbl: UILabel!
    @IBOutlet weak var dropOffLbl: UILabel!
    @IBOutlet weak var rideTimeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
