//
//  LoginVC.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import FirebaseAuth
import CircularSpinner

class LoginVC: UIViewController {

    @IBOutlet weak var textForUsername: UITextField!
    @IBOutlet weak var textForPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // This check for first Time login
        if UserDefaults.standard.object(forKey: "ISLOGIN") as? String == "YES"{
            let tabObj = storyboard!.instantiateViewController(withIdentifier: "CustomNaviVC") as! CustomNaviVC
            self.navigationController?.pushViewController(tabObj, animated: true)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark :- IBAction Methods
    @IBAction func tapOnLoginButton(_ sender: Any) {
        
        self.view.endEditing(true)
        
        // To resgin key board when we tap Login button
        
        if (textForUsername.text?.isEmpty)! || (textForPassword.text?.isEmpty)! {
            
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: kAlertMessage_EmptyAllCheck, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok" , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
    
            CircularSpinner.show("", animated: true, type: .indeterminate, showDismissButton: false, delegate: nil)
           
           FIRAuth.auth()?.signIn(withEmail: textForUsername.text!, password: textForPassword.text!) { (user, error) in
            CircularSpinner.hide()
            if user != nil {
                // User is signed in.
                print("start login success: " + (user?.email)! )
                // set this value for one time login
                UserDefaults.standard.set("YES", forKey: "ISLOGIN")
                UserDefaults.standard.set(user?.uid, forKey: "LOGINUSERVALUE")
                UserDefaults.standard.synchronize()
                
                let tabObj = self.storyboard!.instantiateViewController(withIdentifier: "CustomNaviVC") as! CustomNaviVC
                self.navigationController?.pushViewController(tabObj, animated: true)

            } else {
                // No user is signed in.
                print("No user is signed in.")
            }
           }
        }
    }
    
    //Social APP Logins
    @IBAction func socialLoginAction(_ sender : UIButton) {
        let buttonTag = sender.tag
        if(buttonTag == 1) {
            print("FB")
        }else if(buttonTag == 2) {
            print("Google")
        }else if(buttonTag == 3) {
            print("Link")
        }else {
            print("Twitter")
        }
    }
}
