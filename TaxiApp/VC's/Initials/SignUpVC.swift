//
//  SignUpVC.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import MRCountryPicker
import CircularSpinner

class SignUpVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,MRCountryPickerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var txtName       : UITextField!
    @IBOutlet weak var txtPassword       : UITextField!
    @IBOutlet weak var txtConfirmPassword       : UITextField!
    @IBOutlet weak var txtEmailID        : UITextField!
    @IBOutlet weak var btnProfilePic     : UIButton!
    @IBOutlet weak var txtMobile         : UITextField!
    @IBOutlet weak var btnForCountry: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    // create picker view object
    let picker = UIImagePickerController()
    @IBOutlet weak var countryPicker : MRCountryPicker!
    @IBOutlet weak var countryPhoneCodeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Make profile button view circular
        btnProfilePic.layer.cornerRadius = btnProfilePic.frame.height/2
        btnProfilePic.clipsToBounds = true
        
        //Country Picker Delegate setting
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = false
        countryPicker.setCountry("IN")
        
        picker.delegate = self
        
        //Add border outside country button
        btnForCountry.backgroundColor = .clear
        btnForCountry.layer.cornerRadius = 5
        btnForCountry.layer.borderWidth = 1
        btnForCountry.layer.borderColor = UIColor.lightGray.cgColor
        
        //Add border above picker view
        let border = CALayer()
        border.backgroundColor = UIColor(red: 241.0/255.0, green: 176.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        border.frame = CGRect(x: 0, y: 0, width: countryPhoneCodeView.frame.width, height: 1.0)
        
        countryPhoneCodeView.layer.addSublayer(border)
        countryPicker = MRCountryPicker()

        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Back Button Action
    @IBAction func backAction(_ sender : AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //Action for profile pic button
    @IBAction func selectProfileAction(_sender : AnyObject) {
        self.view.endEditing(true)
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        settingsActionSheet.addAction(UIAlertAction(title:"Photo Library", style:UIAlertActionStyle.default, handler:{ action in
            self.photoFromLibrary()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Camera", style:UIAlertActionStyle.default, handler:{ action in
            self.shootPhoto()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        present(settingsActionSheet, animated:true, completion:nil)
        
    }
    
    //Sign Up Action for email
    @IBAction func createAccountAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if txtName.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please fill all field", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            
            CircularSpinner.show("", animated: true, type: .indeterminate, showDismissButton: false, delegate: nil)

            FIRAuth.auth()?.createUser(withEmail: txtEmailID.text!, password: txtConfirmPassword.text!) { (user, error) in
                
                CircularSpinner.hide()
                if error != nil {
                    
                    print("Error :: \(String(describing: error))")
                    
                    let alertController = UIAlertController(title: "Error", message: String(describing: error!.localizedDescription), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    UserDefaults.standard.setValue(user!.uid, forKey: "LOGINUSERVALUE")
                    
                    //  let base64Str = Utility().convertingStringToBase64(image: self.btnProfilePic.currentImage!)
                    
                    let childUpdates = ["/Users/\(user!.uid)": ["email" : self.txtEmailID.text!,
                                                                "userName" : self.txtName.text!,
                                                                "mobileNumber" : self.txtMobile.text!,
                                                                "country" : self.btnForCountry.currentTitle!,
                                                                "walletAmount":"0.00"]
                    ]
                    FIRDatabase.database().reference().updateChildValues(childUpdates)
                    
                    let tabObj = self.storyboard!.instantiateViewController(withIdentifier: "CustomNaviVC") as! CustomNaviVC
                    self.navigationController?.pushViewController(tabObj, animated: true)
                }
            }
        }
    }
    
    //Action for handling profile pic events
    func photoFromLibrary() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        // picker.popoverPresentationController?.barButtonItem = sender
    }
    
    func shootPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    //MARK: Image View Delegate Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
        
    {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        
        chosenImage = resizeImage(image: chosenImage, newWidth: 300)
        btnProfilePic.setImage(chosenImage, for: .normal)
        
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x:0, y:0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func onTapOnCountryDoneButton(_ sender: Any) {
        
        let bottomOffset = CGPoint(x: 0, y: -70)
        scrollView.setContentOffset(bottomOffset, animated: true)

        UIView.animate(withDuration: 0.5, animations:{
            self.countryPhoneCodeView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width , height: self.countryPhoneCodeView.frame.size.height)
        })
    }
    
    @IBAction func countryPickerAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let topOffset = CGPoint(x: 0, y: 150)
        scrollView.setContentOffset(topOffset, animated: true)
        
        UIView.animate(withDuration: 0.5, animations:{
            
            self.countryPhoneCodeView.frame = CGRect(x: 0, y: self.view.frame.size.height - self.countryPhoneCodeView.frame.size.height, width: self.view.frame.size.width , height: self.countryPhoneCodeView.frame.size.height)
        })
        
    }
    
    //MRCountry Picker Delegate Methods
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        btnForCountry.setTitle("  "+name, for: .normal)
    }

}
