//
//  HistoryVC.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CircularSpinner

class HistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var arrForHistory = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CircularSpinner.show("", animated: true, type: .indeterminate, showDismissButton: false, delegate: nil)
        
        let userID = UserDefaults.standard.string(forKey: "LOGINUSERVALUE")
        
        FIRDatabase.database().reference().child("History").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            // Get user value
            let dataDict = snapshot.value as! NSDictionary
            
            for (_ , element) in dataDict {
                self.arrForHistory.add(element)
            }
            
            self.tableView.reloadData()
            CircularSpinner.hide()

        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButtonTouched(_ sender: AnyObject) {
        //  self.findHamburguerViewController()?.gestureEnabled = true
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    // MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrForHistory.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        cell.pickOffLbl.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "pickup") as? String
        cell.pickOffLbl.sizeToFit()
        
        cell.rideTimeLbl.text = "Not Available" //(arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "delayTime") as? String Need to change
        cell.rideStatusLbl.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "status") as? String
        cell.dropOffLbl.text = "Not Available" //(arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "drop") as? String Need to change
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (((arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "pickup") as? String )?.isEmpty)! {
            return 0
        } else {
            return 278
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}

