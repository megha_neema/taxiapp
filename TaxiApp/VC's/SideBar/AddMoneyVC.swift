//
//  AddMoneyVC.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AddMoneyVC: UIViewController,PayPalPaymentDelegate {

    var payPalConfig = PayPalConfiguration()
    @IBOutlet weak var textForMoney : UITextField!
    var amount : CGFloat = 0.00

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]

        payPalConfig.payPalShippingAddressOption = .none
        
        PayPalMobile.preconnect(withEnvironment: PayPalEnvironmentSandbox)
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  PayPalMobile.preconnect(withEnvironment: environment)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButtonTouched(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    // MARK: Single Payment
    @IBAction func addMoneyAction(_ sender: AnyObject) {
        self.view.endEditing(true)

        if (textForMoney.text?.isEmpty)! {
            let alertController = UIAlertController(title: "Error", message: "Invalid amount,please enter valid amount.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
            return
        }

        
       amount = CGFloat((textForMoney.text! as NSString).doubleValue)
       let item1 = PayPalItem(name: "Sample item", withQuantity: 1, withPrice: NSDecimalNumber(string: textForMoney.text), withCurrency: "USD", withSku: "Hip-00037")
        let items: [Any] = [item1]
        let subtotal: NSDecimalNumber? = PayPalItem.totalPrice(forItems: items)
        let shipping = NSDecimalNumber(string: "0")
        let tax = NSDecimalNumber(string: "0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total: NSDecimalNumber? = subtotal?.adding(shipping).adding(tax)
        let payment = PayPalPayment()
        payment.amount = total!
        payment.currencyCode = "USD"
        payment.shortDescription = "Sample item"
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func saveTransactionDetails(details:NSDictionary) {
        
        let userID = UserDefaults.standard.string(forKey: "LOGINUSERVALUE")
        
        FIRDatabase.database().reference().child("Users").child(userID!).child("walletAmount").observeSingleEvent(of: .value, with: { (snapshot) in

            let value = CGFloat((snapshot.value as! NSString).doubleValue) + self.amount
            
            FIRDatabase.database().reference().child("Users").child(userID!).updateChildValues(["walletAmount": "\(String(describing: value))"])
        })
        
        
       let childUpdates = ["/Transaction_Details/\(String(describing: UserDefaults.standard.string(forKey: "LOGINUSERVALUE")!))/Money_Added/\((details.value(forKey: "response") as! NSDictionary).value(forKey: "id")!))": ["amount": self.textForMoney.text!,
                       "create_time": (details.value(forKey: "response") as! NSDictionary).value(forKey: "create_time")!,
                       "currency_code": "USD",
                       "mobilePlatform": (details.value(forKey: "client") as! NSDictionary).value(forKey: "platform")!,
                       "paymentId":(details.value(forKey: "response") as! NSDictionary).value(forKey: "id")!,
                       "paymentState": (details.value(forKey: "response") as! NSDictionary).value(forKey: "state")!,
                       "product_name": (details.value(forKey: "client") as! NSDictionary).value(forKey: "product_name")!]]
            
            FIRDatabase.database().reference().updateChildValues(childUpdates)
           self.textForMoney.text = ""
    }
    
    // PayPalPaymentDelegate
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.saveTransactionDetails(details:completedPayment.confirmation as NSDictionary)
            
            let alertController = UIAlertController(title: "Payment Complete", message: "Your payment has been completed successfully.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        })
    }
}
