
//
//  MapVC.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import FirebaseAuth
import FirebaseDatabase
import GooglePlaces

class MapVC: UIViewController,GMSMapViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var baseView: GMSMapView!
    @IBOutlet weak var originSearchBar: UISearchBar!
    @IBOutlet weak var destinationSearchBar: UISearchBar!
    var seacrhBarTag = Int()
    
    var sourceLocationStr = String()
    var sourceLocationStrCoordinates = CLLocationCoordinate2D()
    
    @IBOutlet weak var buttonView: UIView!
    
    var destinationLocationStr = String()
    var destinationLocationStrCoordinates = CLLocationCoordinate2D()
    
    var flag = true
    
    var overlay : UIView = UIView()
    var alertController = UIAlertController()
    var bookingKey : String = ""
    
    var arrForCarLatLong = NSMutableArray()
    var arrForBikeLatLong = NSMutableArray()
    
    let locationManager = CLLocationManager()
    var markers = [GMSMarker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseView.delegate = self
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(MapVC.pushViewAfterDelay), name: NSNotification.Name(rawValue: "MenuSelected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MapVC.rideConfirmAction), name: NSNotification.Name(rawValue: "RideConfirm"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MapVC.rideCancellationAction), name: NSNotification.Name(rawValue: "RideCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MapVC.rideReviewAction), name: NSNotification.Name(rawValue: "RideReview"), object: nil)
        getDriverLocation()
        
        overlay = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height)))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let swipeGestureUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeGestureUp.direction = UISwipeGestureRecognizerDirection.up
        swipeGestureUp.delegate = self
        buttonView.addGestureRecognizer(swipeGestureUp)
        
        let swipeGestureDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeGestureDown.direction = UISwipeGestureRecognizerDirection.down
        swipeGestureDown.delegate = self
        buttonView.addGestureRecognizer(swipeGestureDown)
    }
    
    func addMotorCycleMarker(lat: Double, long: Double)
    {
        for pin: GMSMarker in markers {
            if pin.userData as! String == "car" {
                pin.map = nil
            }
        }
        let position = CLLocationCoordinate2DMake(lat,long)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage.init(named: "motorcycle")
        marker.userData = "motorcycle"
        marker.map = baseView
        markers.append(marker)
    }
    
    func addCarMarker(lat: Double, long: Double)
    {
        for pin: GMSMarker in markers {
            if pin.userData as! String == "motorcycle" {
                pin.map = nil
            }
        }
        let position = CLLocationCoordinate2DMake(lat,long)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage.init(named: "car")
        marker.userData = "car"
        marker.map = baseView
        markers.append(marker)
    }
    
    func getDriverLocation(){
        
        FIRDatabase.database().reference().child("geofire").observeSingleEvent(of: .value, with: { (snapshot) in
            
            // Get user value
            let dataDict = snapshot.value as! NSDictionary
            print("dataDict==>%@",dataDict)
            for (key, _) in dataDict {
                
                if key as! String == "Bike" {
                    
                    let bikeDict = dataDict.value(forKey: "Bike") as! NSDictionary
                    
                    for (key, _) in bikeDict
                    {
                        self.arrForBikeLatLong.add((bikeDict.value(forKey: key as! String) as! NSDictionary).value(forKey: "l") ?? "")
                    }
                    print("arrForBikeLatLong==>%@",self.arrForBikeLatLong)
                }else if key as! String == "Van" {
                    
                    let carDict = dataDict.value(forKey: "Van") as! NSDictionary
                    
                    for (key, _) in carDict
                    {
                        self.arrForCarLatLong.add((carDict.value(forKey: key as! String) as! NSDictionary).value(forKey: "l") ?? "")
                    }
                    print("arrForCarLatLong==>%@",self.arrForCarLatLong)
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func menuButtonTouched(_ sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    @IBAction func actionOnLowerView(_ sender: UIButton){
        
        switch sender.tag {
        //tag 0 means Bike
        case 0:
            for  i in 0..<arrForBikeLatLong.count {
                addMotorCycleMarker(lat: ((arrForBikeLatLong.object(at: i)) as! NSArray).object(at: 0) as! Double
                    , long: ((arrForBikeLatLong.object(at: i)) as! NSArray).object(at: 1) as! Double)
            }
            
            break
        //tag 1 means CAR
        case 1:
            for  i in 0..<arrForCarLatLong.count {
                addCarMarker(lat: ((arrForCarLatLong.object(at: i)) as! NSArray).object(at: 0) as! Double
                    , long: ((arrForCarLatLong.object(at: i)) as! NSArray).object(at: 1) as! Double)
            }
            break
            
        //tag 2 means Ride later
        case 2:
            
            break
            
        //tag 3 means Ride Now
        case 3:
            self.view.isUserInteractionEnabled = true
            let rideNowView = ConfirmRideView()
            self.view.addSubview(overlay)
            self.view.addSubview(rideNowView)
            break
            
        default:
            
            break
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        let location: CLLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        
        CLGeocoder().reverseGeocodeLocation(location , completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
                marker.title = pm?.name!
                marker.snippet = pm?.subLocality!
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        marker.map!.selectedMarker = marker
        return true
    }
    
    func respondToSwipeGesture(_ gestureRecognizer: UISwipeGestureRecognizer) {
        
        let estimationView = EstimationView()
        
        switch gestureRecognizer.direction {
        case UISwipeGestureRecognizerDirection.up:
            UIView.animate(withDuration: 0.5, animations:{
                self.view.frame = CGRect(x: 0, y: -190, width: self.view.frame.size.width , height: self.view.frame.size.height)
                self.view.addSubview(estimationView)
                
            })
            
        case UISwipeGestureRecognizerDirection.down:
            UIView.animate(withDuration: 0.5, animations:{
                estimationView.removeFromSuperview()
                self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
            })
            
        default:
            break
        }
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension MapVC:CLLocationManagerDelegate {
    
    //Mark:- Location manager delegate mthods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let age:TimeInterval = (location?.timestamp.timeIntervalSinceNow)!
        if age > 120 {
            return
        }
        if Int((location?.horizontalAccuracy)!) < 0 {
            return
        }
        
        if flag {
            
            let position = CLLocationCoordinate2DMake((location?.coordinate.latitude)!,(location?.coordinate.longitude)!)
            
            CLGeocoder().reverseGeocodeLocation(location! , completionHandler: {(placemarks, error) in
                if (error != nil) {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    return
                }
                
                if (placemarks?.count)! > 0 {
                    let pm = placemarks?[0]
                    self.sourceLocationStr = (pm?.name!)!
                    self.originSearchBar.text = self.sourceLocationStr
                    let marker = GMSMarker(position: position)
                    marker.title = pm?.locality
                    marker.snippet = pm?.subLocality
                    marker.icon = GMSMarker.markerImage(with: UIColor.yellow)
                    marker.userData = "origin"
                    marker.map = self.baseView
                    
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
            
            
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 20.0)

            sourceLocationStrCoordinates = position
            self.baseView?.animate(to: camera)
            flag = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    

}

extension MapVC {
    //Mark :- Notification Action
    //Mark :- Sidebar Action
    func pushViewAfterDelay(withNotification notification : NSNotification){
        
        print(notification)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "MenuSelected"), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(MapVC.pushViewAfterDelay), name: NSNotification.Name(rawValue: "MenuSelected"), object: nil)
        
        
        let temp = notification.object as! String
        
        let intTag = Int(temp)!
        
        switch intTag {
        case 0:
            if (self.navigationController?.topViewController?.isKind(of: MapVC.self))! {
                break
            }
            let findProObj = storyboard!.instantiateViewController(withIdentifier: "MapVC") as! MapVC
            self.navigationController?.pushViewController(findProObj, animated: true)
            break
            
            
        case 1:
            if (self.navigationController?.topViewController?.isKind(of: HistoryVC.self))! {
                break
            }
            let findProObj = storyboard!.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
            self.navigationController?.pushViewController(findProObj, animated: true)
            break
            
        case 2:
            if (self.navigationController?.topViewController?.isKind(of: AddMoneyVC.self))! {
                break
            }
            let findProObj = storyboard!.instantiateViewController(withIdentifier: "AddMoneyVC") as! AddMoneyVC
            self.navigationController?.pushViewController(findProObj, animated: true)
            break
            
        case 3:
            
            let alert = UIAlertController(title: "Logout Confirmation", message:"Are you sure you want to logout?" , preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { action in
                switch action.style{
                case .default:
                    try! FIRAuth.auth()!.signOut()
                    AppDelegate().appDelegate().logout()
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
            
        default:
            break
        }
    }
 
    //Mark :- Confirm Ride Action
    func rideConfirmAction(withNotification notification : NSNotification){
        self.overlay.removeFromSuperview()
        let btnTag = notification.userInfo?["senderButton"] as! String
        
        if btnTag == "2" {
            
            saveBookingDetails()
            
            alertController = UIAlertController(title: nil, message: "Finding Driver...", preferredStyle: .alert)
            
            let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            
            spinnerIndicator.center = CGPoint(x: 35.0, y: 33.0)
            spinnerIndicator.color = UIColor(colorLiteralRed: 241.0/255, green: 176.0/255, blue: 79.0/255, alpha: 1)
            spinnerIndicator.startAnimating()
            
            alertController.view.addSubview(spinnerIndicator)
            self.present(alertController, animated: false, completion: nil)
        }
    }
    
    //Mark :- Cancel Ride Action
    func rideCancellationAction(withNotification notification : NSNotification){
        self.overlay.removeFromSuperview()
        let btnTag = notification.userInfo?["senderButton"] as! String
        if btnTag == "2" {
            if let reason = notification.userInfo?["reason"] as? String {
                let databaseRef  = FIRDatabase.database().reference().child("Booking").child(self.bookingKey)
                databaseRef.updateChildValues(["feedback": reason,                                                                                                                 "status": "cancel"])
            }
        }
    }
    
    //Mark :- Review Ride Action
    func rideReviewAction(withNotification notification : NSNotification){
        self.overlay.removeFromSuperview()
        if notification.userInfo != nil {
            let databaseRef  = FIRDatabase.database().reference().child("Booking").child(self.bookingKey)
            databaseRef.updateChildValues(["feedback": "\(String(describing: (notification.userInfo?["reason"])!))",                                                                                                                 "rideRating": "\(String(describing: (notification.userInfo?["rank"])!))"])
        }
        self.createBookingHistory()
    }
}

//Mark : - Complete Ride Flow Action
extension MapVC {

    func saveBookingDetails() {
        
        let databaseRef = FIRDatabase.database().reference()
        
        databaseRef.child("Users").child(UserDefaults.standard.string(forKey: "LOGINUSERVALUE")!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let dataDict = snapshot.value as! NSDictionary
            
            let newRef = databaseRef.child("Booking").childByAutoId()
            self.bookingKey = newRef.key
            print("Key :: \(self.bookingKey)")
            UserDefaults.standard.setValue(self.bookingKey, forKey: "BookingID")
            
            let childUpdates = ["/Booking/\(self.bookingKey)": ["bookingId": self.bookingKey,
                                                                "customerId": UserDefaults.standard.string(forKey: "LOGINUSERVALUE"),
                                                                "customerName": dataDict.value(forKey: "userName"),
                                                                "customerPhoneNo": dataDict.value(forKey: "mobileNumber"),
                                                                "customerToken": "",
                                                                "delayTime": "",
                                                                "destination": self.destinationLocationStr,
                                                                "destinationLatitude": self.destinationLocationStrCoordinates.latitude,
                                                                "destinationLongitude": self.destinationLocationStrCoordinates.longitude,
                                                                "distance": "",
                                                                "driverId": "",
                                                                "driverName": "",
                                                                "driverPhoneNo": "",
                                                                "driverToken": "",
                                                                "feedback": "",
                                                                "pickup": self.sourceLocationStr,
                                                                "pickupLatitude": self.sourceLocationStrCoordinates.latitude,
                                                                "pickupLongitude": self.sourceLocationStrCoordinates.longitude,
                                                                "rideRating": "",
                                                                "status": "request",
                                                                "totalTime": "",
                                                                "type": "",
                                                                "paymentMode" :UserDefaults.standard.string(forKey: "PaymentMode"),
                                                                "rideCharges":""]]
            
            FIRDatabase.database().reference().updateChildValues(childUpdates)

            // Listen for new comments in the Firebase database
            FIRDatabase.database().reference().child("Booking").child("\(self.bookingKey)").observe(.childChanged, with: { (snapshot) in
                
                if snapshot.key == "status" {
                    if snapshot.value as! String  == "confirmed" {
                        self.overlay.removeFromSuperview()
                        self.alertController.dismiss(animated: true, completion: nil);
                        self.showBookingDetailsAlert()
                    } else if snapshot.value as! String  == "completed" {
                        let reviewRideView = ReviewRideView()
                        self.view.addSubview(self.overlay)
                        self.view.addSubview(reviewRideView)
                    } else if snapshot.value as! String  == "progress" {
                          self.alertController.dismiss(animated: true, completion: nil)
                    } else if snapshot.value as! String  == "cancel" {
                        self.createBookingHistory()
                    }
                }
            })
        })
    }
    
    func createBookingHistory() {
        FIRDatabase.database().reference().child("Booking").child(
            
            self.bookingKey).observeSingleEvent(of: .value, with: { (snapshot) in
       
                let dataDict = snapshot.value as! NSDictionary
                
                let childUpdates = ["/History/\(UserDefaults.standard.string(forKey: "LOGINUSERVALUE")!)/\(self.bookingKey)":
                    dataDict]
                
                FIRDatabase.database().reference().updateChildValues(childUpdates)
                
            })
    }
    
    func showBookingDetailsAlert() {
        
        FIRDatabase.database().reference().child("Booking").child("\(self.bookingKey)").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let dataDict = snapshot.value as! NSDictionary
            
            self.alertController = UIAlertController(title: "Booking Details", message: "Driver \(String(describing: dataDict.value(forKey: "driverName")!)) will arrive shortly.\nPhone Number:\(String(describing:dataDict.value(forKey: "driverPhoneNo")!))", preferredStyle: .alert)
            
            // Create the actions
            let callAction = UIAlertAction(title: "CALL", style: UIAlertActionStyle.default) {
                UIAlertAction in
                if let url = URL(string: "tel://\(String(describing:dataDict.value(forKey: "driverPhoneNo")))") {
                    UIApplication.shared.openURL(url)
                }
            }
            
            let cancelAction = UIAlertAction(title: "CANCEL RIDE", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                
                if dataDict.value(forKey: "status") as? String == "confirmed" || dataDict.value(forKey: "status") as? String == "request" {
                    let cancelRideView = CancellationView()
                    self.view.addSubview(self.overlay)
                    self.view.addSubview(cancelRideView)
                }
            }
            
            // Add the actions
            self.alertController.addAction(callAction)
            self.alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(self.alertController, animated: true, completion: nil)
        })
    }
}

extension MapVC: GMSAutocompleteViewControllerDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        seacrhBarTag = searchBar.tag
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    func dropPinZoomIn(_ placemark: GMSPlace){

        let marker = GMSMarker(position: placemark.coordinate)
   
        if seacrhBarTag == 1 {
            for pin: GMSMarker in markers {
                if pin.userData as! String == "source" {
                    pin.map = nil
                }
            }
            sourceLocationStr = placemark.name
            sourceLocationStrCoordinates = placemark.coordinate
            marker.icon = GMSMarker.markerImage(with: UIColor.red)
            marker.title = placemark.name
            marker.userData = "source"
        }else {
            for pin: GMSMarker in markers {
               if pin.userData as! String == "destination" {
                    pin.map = nil
                }
            }
            destinationLocationStr = placemark.name
            destinationLocationStrCoordinates = placemark.coordinate
            marker.icon = GMSMarker.markerImage(with: UIColor.green)
            marker.title = placemark.name
            marker.userData = "destination"
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: placemark.coordinate.latitude, longitude: placemark.coordinate.longitude, zoom: 20.0)
        marker.map = baseView
        markers.append(marker)
        self.baseView?.animate(to: camera)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if seacrhBarTag == 1 {
            originSearchBar.text = place.name
        }else {
            destinationSearchBar.text = place.name
        }
        dropPinZoomIn(place)
        
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress!))")
        print("Place Coordinates: \(String(describing: place.coordinate))")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
}

