//
//  AppDelegate.swift
//  TaxiApp
//
//  Created by Consagous on 03/05/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func appDelegate () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // this code for create white navigation title
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor(red: 241.0/255.0, green: 176.0/255.0, blue: 79.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor(red: 241.0/255.0, green: 176.0/255.0, blue: 79.0/255.0, alpha: 1.0)]

         GMSServices.provideAPIKey("AIzaSyBMzzzpNiKfLorzkbn3SSSolxFqXgxLSyU")
         GMSPlacesClient.provideAPIKey("AIzaSyBMzzzpNiKfLorzkbn3SSSolxFqXgxLSyU")
         FIRApp.configure()
        
         PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                                PayPalEnvironmentSandbox: "ASUjYE3O8wdOHyA2IbwiG7veA-dSxT2HT_EY7BQkPhYBvPZz6TnNtwvQX8lpo_U8RTEGoeCyyn6zrKrL"])
         return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func logout() {
        
        UserDefaults.standard.set("NO", forKey: "ISLOGIN")
        UserDefaults.standard.set(nil, forKey: "LOGINUSERVALUE")
        UserDefaults.standard.synchronize()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
    }
}

