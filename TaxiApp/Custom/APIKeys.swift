//
//  APIKeys.swift
//  SupplyMedicine
//
//  Created by GalaxyWeblinks on 12/21/15.
//  Copyright © 2015 GalaxyWeblinks. All rights reserved.
//

import Foundation
//import Alamofire

let kSessionUsername    =   "SessionUsername"
let kSessionPassword    =   "SessionPassword"
let kSessionToken       =   "a_t"



//http://dev.galaxyweblinks.com/bnd_member_app/member/logout

//Local server
//let kAPI_SERVERBASEURL = "http://hoop-env.us-west-2.elasticbeanstalk.com/hoop/"

// Live Server
let kAPI_SERVERBASEURL = "https://www.myhoopapp.com/hoop/"


//Path

let kAPI_GETCOUNTRIES  = "geographic/countries"
let kAPI_GETSTATES  = "geographic/states"
let kAPI_GETCITIES  = "geographic/cities"


let kAPI_SENDINVITE                = "contacts/sendInvite"

let kAPI_SIGNUP                =  "signup/register/"
let kAPI_SETNEWPASSWORD         = "login/forgotPassword/setNewPassword"
let kAPI_SETCHANGEPASSWORD      = "login/changePassword"

let kAPI_GETGROUPS              =  "groups/get"
let kAPI_CREATEGROUPS           = "groups/create"
let kAPI_DELETEGROUPS           = "groups/delete"
let kAPI_GETGROUPSMEMBERS = "groups/members/get"
let kAPI_ADDGROUPSMEMBERS = "groups/members/add"
let kAPI_UPDATEGRUPEMEMBERS = "groups/members/update"
let kAPI_GETUSERRECENTCONTACT = "groups/members/recent"

let kAPI_GETRECENTUPDATE = "notifications/userDetailsUpdate"


let kAPI_GETOWNCARDS         =  "owncards/get"
let kAPI_GETRECEIVEDCARDS         = "owncards/getReceivedCards"
let kAPI_DELETECARDS         = "owncards/delete"

let kAPI_USERLOGOUT         = "login/logout"
let kAPI_CREATEOWNCARDS  =  "owncards/add"
let kAPI_SHARERECIEVEDCARDS  = "owncards/addContactsAndGroupMembersFromReceivedCards"
let kAPI_DELETERECIEVEDCARDS  = "owncards/deleteDeclinedCardsSharing"

let kAPI_SHAREMULTIPLECARDSWITHUSERS  = "owncards/shareWithMultipleHoopUsers"

let kAPI_FORGOTPASSWORD         = "login/forgotPassword"

let kAPI_REGISTER_LOGIN         =   "login/authenticate"
let kAPI_REGISTER_OTP         = "signup/activateWithOTP"
let kAPI_SENDUSERDETAIL  = "userDetails/all"
let kAPI_SENDUSERDETAILUPDATE  = "userDetails/all/update"

let kAPI_ADDBUISSNESSDETAILS  = "userDetails/addBusiness"
let kAPI_ADDCOMPANYDETAILS  = "userDetails/addCompany"
let kAPI_ADDEDUCATIONDETAILS  = "userDetails/addEducation"

let kAPI_ADDUSERBUISNESS  = "userDetails/addBusiness"
let kAPI_USERREQUESTMISSINGDETAILS  = "notifications/multipleUserDetailsMissingSend"

let kAPI_CONTACTSHARE  = "contacts/share"
let kAPI_CONTACTGETNONHOOPANDHOOP  = "contacts/connectedHoopAndNonHoopUsers"
let kAPI_CONTACTFILTER  = "connectedUsers/filtered/combined"
let kAPI_UPDATEUSERPROFILE  = "image/uploadToAws"

let kAPI_GETUSERDETAILS = "contacts/controlledUserDetails?"
let kAPI_GETUSERMUTUALFRIENDS = "contacts/mutualFriends?"

let kCategory_name      = "category_name"


//For Login and Sign Up



let kDigest = "digest"
let kEmail = "email"
let kUserName = "username"
let kMobile = "mobile"

let kUserDetails = "User Details"

// For Basic details
let kBasicDetails = "Basic Details"
let kdob = "dob"
let kfirst_name = "first_name"
let klast_name = "last_name"
let kgender = "gender"
let kimage_url = "image_url"
let kmiddle_name = "middle_name"
let kprefix = "prefix"
let kprofession = "profession"



let kBusinessDetailsList = "Business Details List"
let kEducationDetailsList = "Education Details List"
let kaddress_line1 = "address_line1"
let kaddress_line2 = "address_line2"
let kcity = "city"
let kcountry = "country"
let kcourse_name = "course_name"
let keducation_level = "education_level"
let kinstitute_name = "institute_name"
let kstate = "state"
let kyear = "year"
let kzipcode = "zipcode"

let kEmploymentDetailsList = "Employment Details List"
let kcompany_name = "company_name"
let kcompany_type = "company_type"
let kcompany_website = "company_website"
let kdesignation = "designation"
let kwork_email_id = "work_email_id"
let kwork_phone_no = "work_phone_no"

let kGeneralDetails = "General Details"
let kbusiness = "business"
let kdoing_multiple_courses = "doing_multiple_courses"
let kemployee = "employee"
let kfull_time_freelancer = "full_time_freelancer"
let kholding_multiple_businesses = "holding_multiple_businesses"
let kpart_time_freelancer = "part_time_freelancer"
let kstudent = "student"
let kunemployed = "unemployed"
let kworking_for_multiple_companies = "working_for_multiple_companies"

let kPersonalContactDetails = "Personal Contact Details"
let khome_telephone_no = "home_telephone_no"
let kmobile_no = "mobile_no"
let kpersonal_email_id = "personal_email_id"
let kPersonalSocialMediaDetails  = "Personal Social Media Details"
let kfacebook = "facebook"

