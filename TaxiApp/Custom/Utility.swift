//
//  Uitility.swift
//  TaxiApp
//
//  Created by Megha on 5/8/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import CoreLocation

class Utility: NSObject {
    
    // convert UIImage to base64 image string
    func convertingStringToBase64(image : UIImage) -> String{
        let imageData : Data = UIImagePNGRepresentation(image)! as Data
        let strBase64 = imageData.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
    
        return strBase64
    }
}
