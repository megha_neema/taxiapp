//
//  Constant.swift
//  BrandNewDay
//
//  Created by Galaxy on 18/07/16.
//  Copyright © 2016 Galaxy. All rights reserved.
//


import Foundation
import UIKit
import CoreBluetooth

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


#if DEBUG
let kDEBUG_MODE = true
let kSERVER_BASE_URL  = "google.com"
#else
let kDEBUG_MODE = false
#endif

// UUID
//let kUUID = Utility().deviceUUID()

// IPHONE SCREEN WIDTH AND HEIGHT BOUND CONDITION


let TRANSFER_SERVICE_UUID = "E20A39F4-73F5-4BC4-A12F-17D1AD666661"
let TRANSFER_CHARACTERISTIC_UUID = "08590F7E-DB05-467E-8757-72F6F66666D4"
let NOTIFY_MTU = 20

let transferServiceUUID = CBUUID(string: TRANSFER_SERVICE_UUID)
let transferCharacteristicUUID = CBUUID(string: TRANSFER_CHARACTERISTIC_UUID)




let kIS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let kIS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let kIS_RETINA = UIScreen.main.scale >= 2.0
let kSYSTEM_VERSION = Float(UIDevice.current.systemVersion)

let kSCREEN_X = UIScreen.main.bounds.origin.x
let kSCREEN_Y = UIScreen.main.bounds.origin.y
let kSCREEN_WIDTH = UIScreen.main.bounds.size.width
let kSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let kSCREEN_MAX_LENGTH = max(kSCREEN_WIDTH, kSCREEN_HEIGHT)
let kSCREEN_MIN_LENGTH = min(kSCREEN_WIDTH, kSCREEN_HEIGHT)

let kIS_IPHONE_4_OR_LESS = (kIS_IPHONE && kSCREEN_MAX_LENGTH < 568.0)
let kIS_IPHONE_5 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 568.0)
let kIS_IPHONE_6 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 667.0)
let kIS_IPHONE_6P = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 736.0)

// IOS version check
let kIS_OS_7 = ((kSYSTEM_VERSION >= 7.0) && (kSYSTEM_VERSION < 8.0))
let kIS_OS_7_OR_LATER = ((kSYSTEM_VERSION >= 7.0))
let kIS_OS_8_OR_LATER = (kSYSTEM_VERSION >= 8.0)

let kRATIO_WIDTH_4 = 0.85
let kRATIO_HEIGHT_4 = 0.72
let kRATIO_5 = 0.85
let kRATIO_6 = 1.0
let kRATIO_6_PLUS = 1.10

//for Padding
let padding: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 40))
////////////// Common Color Code ///////////////
let colorClear = UIColor.clear
let colorWhite = UIColor.white
let colorBlack = UIColor.black
let colorRed = UIColor.red
let colorGray = UIColor.gray
let colorDarkGray = UIColor.darkGray
let colorLightGray = UIColor.lightGray
let colorYellow = UIColor.yellow
let colorGreen = UIColor.green
let colorCyan = UIColor.cyan

let colorThemeDark = UIColor(red: 0.0/255.0, green: 93.0/255.0, blue: 97.0/255.0, alpha: 1)
let colorThemeMedium = UIColor(red: 20.0/255.0, green: 160.0/255.0, blue: 160.0/255.0, alpha: 1)
let colorThemeLight = UIColor(red: 224.0/255.0, green: 254.0/255.0, blue: 255.0/255.0, alpha: 1)
let colorTextColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)


let colorGreenDelivered = UIColor(red: 26.0/255.0, green: 171.0/255.0, blue: 0.0/255.0, alpha: 1)
let colorOrangePending = UIColor(red: 232.0/255.0, green: 172.0/255.0, blue: 81.0/255.0, alpha: 1)
let colorRedCancelled = UIColor(red: 226.0/255.0, green: 35.0/255.0, blue: 42.0/255.0, alpha: 1)
let colorBlueInTransit = UIColor(red: 58.0/255.0, green: 126.0/255.0, blue: 255.0/255.0, alpha: 1)

// Constant Images
let imageNavigationBar      = UIImage(named: "navigation-bar")
let imageHelpRightBarButton = UIImage(named: "imgHelpRightBarButton")
let imageAddRightBarButton  = UIImage(named: "imgAddButton")
let imageChangePasswordRightBarButton  = UIImage(named: "iconChangePassword")
let imageMenuLeftBarButton  = UIImage(named: "imgMenuLeftBarButton")
let imageBackLeftBarButton  = UIImage(named: "imgBackLeftBarButton")
let imageNextBarButton      = UIImage(named: "imgNextToolBarButton")
let imageBackBarButton      = UIImage(named: "imgBackToolBarButton")
let imageCheckMarkUncheck   = UIImage(named: "iconCheckbox")
let imageCheckMarkCheck     = UIImage(named: "iconSelectedCheckbox")
let imageDropDown           = UIImage(named: "imgDropDownIcon")
let imageRadioUncheck       = UIImage(named: "iconRadioButton")
let imageRadioCheck         = UIImage(named: "iconSelectedRadioButton")
let imageCellBackground     = UIImage(named: "imgCellPatternBG")
let imageCrossRightBarButton = UIImage(named: "iconCross")

// TextField left padding frame.
let textPaddingFrame = CGRect(x: 0, y: 0, width: 10, height: 10)


