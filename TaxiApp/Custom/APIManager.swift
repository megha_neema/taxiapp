



import Foundation
import Alamofire
import ReachabilitySwift
import SwiftyJSON

class APIManager {
    var reachability: Reachability?
    
    //var alamofireObj: Alamofire?
    
    var delegate :AnyObject?
    var callbackSelector : Selector?
    
    /*!
     @brief Created By   : Sumit
     @brief Date         : 23-Dec-2015
     @brief Description  : Sends Asynchronous POST request on input URL and respond back to call back selector method.
     @brief Update by    : --
     @brief Update Date  : --
     @brief Reason       : Method to send POST request to server and respond back call back selector.
     */
    
    
    func initWithDelegate(_ aDelegate : AnyObject,aCallbackSel : Selector) -> AnyObject {
        
        self.delegate = aDelegate;
        self.callbackSelector = aCallbackSel;
        
        return self
    }
    
    func alamofireGet(url:NSString) {
        let strNet: String = "YES" //= startNetworkReachabilityObserver()
        
        if strNet == "YES" {
            let todosEndpoint: String = "\(kAPI_SERVERBASEURL)" + "\(url)"
            
              let urlNew:String = todosEndpoint.replacingOccurrences(of: " ", with: "%20")
            print("GETREQUEST===>",urlNew)
            
            APIManager.Manager.request(urlNew)
                .responseJSON { response in
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET on /todos/1")
                      //  AppDelegate().appDelegate().hideHudView()
                       // self.delegate?.perform(self.callbackSelector!, with:response)
                        print(response.result.error!)
                        return
                    }
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                      //  AppDelegate().appDelegate().hideHudView()
                       // self.delegate?.perform(self.callbackSelector!, with:response)
                        return
                    }
                    // get and print the title
                    guard let todoTitle = json["title"] as? String else {
                        print("Could not get todo title from JSON")
                        print("SERVERRESPONSE==>: \(String(describing: response.result.value))")
                        
                        _ = self.delegate?.perform(self.callbackSelector!, with:response.result.value)
                        
                       // self.delegate?.perform(self.callbackSelector!, with:response)
                        return
                    }
                    print("The title is: " + todoTitle)
            }
        }else{
            let alert = UIAlertController(title: "Alert", message: "Internet connection not available.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           // self.present(alert, animated: true, completion: nil)
            return
        }
        

    }
    
    func alamofirePost(_ dictRequest:NSMutableDictionary,url:NSString) {
        
        
        var strNet: String = "YES" // = startNetworkReachabilityObserver()
        strNet = "YES"
        if strNet == "YES"{
            
            
            let dictReq: [String: Any] = dictRequest.mutableCopy() as! [String : Any]
            
            
            do {
                let data = try JSONSerialization.data(withJSONObject:dictReq, options:[])
                let dataString = String(data: data, encoding: String.Encoding.utf8)!
                print("ValueRequest===\(dataString)")
                // do other stuff on success
            } catch {
                print("JSON serialization failed:  \(error)")
            }

           
            
            let todosEndpoint: String = "\(kAPI_SERVERBASEURL)" + "\(url)"
             print("RequestURL===\(todosEndpoint)")
            
            APIManager.Manager.request(todosEndpoint, method: .post, parameters: dictReq, encoding: JSONEncoding(options: []))
                .responseJSON { response in
                    guard response.result.error == nil else {
                        
                        // got an error in getting the data, need to handle it
                        print("error calling POST on /todos/1")
                        print(response.result.error!)
                        _ = self.delegate?.perform(self.callbackSelector!, with:response)
                        
                        return
                    }
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        _ = self.delegate?.perform(self.callbackSelector!, with:response)
                        
                        print("Error: \(String(describing: response.result.error))")
                        return
                    }
                    // get and print the title
                    guard let todoTitle = json["title"] as? String else {
                        
                        let storage = HTTPCookieStorage.shared
                        
                        for cookie in storage.cookies! {
                            
                            self.setCookie(cookie: cookie)
                            break
                         
                        }
                        
                       
                        print("SERVERRESPONSE==>: \(String(describing: response.result.value))")
                        
                     _ = self.delegate?.perform(self.callbackSelector!, with:response.result.value)
                        
                        return
                    }
                    print("The title is: " + todoTitle)
            }
        }else{
            let alert = UIAlertController(title: "Alert", message: "Internet connection not available.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           // self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    
    
    func alamofirePostNew(_ dictRequest:NSMutableDictionary,URLNEw:NSString) {
        
        
        var strNet: String = "YES" // = startNetworkReachabilityObserver()
        strNet = "YES"
        if strNet == "YES"{
            
            
//            let dictReq: [String: Any] = dictRequest.mutableCopy() as! [String : Any]
            var strTemp = String()
            
            do {
                let data = try JSONSerialization.data(withJSONObject:dictRequest, options:[])
                let dataString = String(data: data, encoding: String.Encoding.utf8)!
                
                strTemp = dataString
                
                
                
                // do other stuff on success
                
            } catch {
                print("JSON serialization failed:  \(error)")
            }
            
            print("ValueRequest===\(strTemp)")
            
            /*
             {"ownerCardId":1351,"ownerUserName":"kaku@gmail.com","recipientUserIds":[1035,1038]}
             */
             let todosEndpoint: String = "\(kAPI_SERVERBASEURL)" + "\(URLNEw)"
            
            print("ValueRequestURL===\(todosEndpoint)")
            
            var request = URLRequest(url: URL(string: todosEndpoint)!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")  // the request is JSON
            request.setValue("application/json", forHTTPHeaderField: "Accept")        // the expected response is also JSON
            request.httpBody = strTemp.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                var dict = NSDictionary()
                if let dataFromString = responseString?.data(using: .utf8, allowLossyConversion: false) {
                    let json = JSON(data: dataFromString)
                    dict = json.object as! NSDictionary
                    
                   _ = self.delegate?.perform(self.callbackSelector!, with:dict)
                }
                print("responseString = \(String(describing: responseString))")
            }
            task.resume()
            
        }else{
            let alert = UIAlertController(title: "Alert", message: "Internet connection not available.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            // self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    
    
    func alamofireDelete() {
        let firstTodoEndpoint: String = "https://jsonplaceholder.typicode.com/todos/1"
        APIManager.Manager.request(firstTodoEndpoint, method: .delete)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling DELETE on /todos/1")
                    print(response.result.error!)
                    return
                }
                print("DELETE ok")
        }
    }


    
 
    
    private static var Manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "www.myhoopapp.com": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    func setCookie (cookie:HTTPCookie)
    {
        UserDefaults.standard.set(cookie.properties, forKey: "kCookie")
        UserDefaults.standard.synchronize()
    }
    
    func getCookie () -> HTTPCookie
    {
        let cookie = HTTPCookie(properties: UserDefaults.standard.object(forKey: "kCookie") as! [HTTPCookiePropertyKey : Any])
        return cookie!
    }
}
