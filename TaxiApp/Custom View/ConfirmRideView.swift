//
//  ConfirmRideView.swift
//  TaxiApp
//
//  Created by Megha on 5/16/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class ConfirmRideView: UIView {
    
    fileprivate var mainView: UIView!
    fileprivate let nibName = "ConfirmRide"

    
    @IBOutlet var collectionButtons: [UIButton]!

    // MARK: - view lifecycle
    public override init(frame: CGRect) {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        super.init(frame: CGRect(x: 0, y: height - 261, width: UIScreen.main.bounds
 .width, height: 261))
        xibSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //custom logic goes here
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func xibSetup() {
        mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(mainView)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    @IBAction func PaymentModeAction(_ sender: UIButton) {
        
        for button : UIButton in collectionButtons {
            if button.isSelected == true {
                button.isSelected = false
            }
        }
        
        if sender.isSelected == true{
            sender.isSelected = false
        }else
        {
            sender.isSelected = true
            if sender.tag == 1 {
                UserDefaults.standard.set("Cash", forKey: "PaymentMode")
            }else {
                UserDefaults.standard.set("Wallet", forKey: "PaymentMode")
            }
         }
    }
    
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        sender.superview?.superview?.superview?.removeFromSuperview()
        let notificationName = Notification.Name("RideConfirm")
        NotificationCenter.default.post(name: notificationName, object: nil , userInfo:["senderButton":"\(sender.tag)"])
    }
}
