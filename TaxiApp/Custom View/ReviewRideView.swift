//
//  ReviewRideView.swift
//  TaxiApp
//
//  Created by Megha on 5/17/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import Firebase

class ReviewRideView: UIView,FloatRatingViewDelegate,UITextViewDelegate {

    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var textForUser: UILabel!
    @IBOutlet weak var textForReview: UITextView!
    
    @IBOutlet weak var floatRatingView: FloatRatingView!
    fileprivate var mainView: UIView!
    fileprivate let nibName = "ReviewRideView"
    var rate: Float = 0.0
    
    // MARK: - view lifecycle
    public override init(frame: CGRect) {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        super.init(frame: CGRect(x: 0, y: height - 280, width: UIScreen.main.bounds
            .width, height: 280))
        xibSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //custom logic goes here
        profileBtn.layer.cornerRadius = profileBtn.frame.height/2
        profileBtn.clipsToBounds = true
        textForReview.delegate = self
        
        // Required float rating view params
        self.floatRatingView.emptyImage = UIImage(named: "star_unchecked")
        self.floatRatingView.fullImage = UIImage(named: "star_checked")
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.delegate = self
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.rating = 1
        self.floatRatingView.editable = true
        self.floatRatingView.halfRatings = false
        self.floatRatingView.floatRatings = false
        
        FIRDatabase.database().reference().child("Booking").child(UserDefaults.standard.string(forKey: "BookingID")!).child("driverName").observeSingleEvent(of: .value, with: { (snapshot) in
            
        self.textForUser.text = snapshot.value as? String
        })
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func xibSetup() {
        mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(mainView)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        superview?.endEditing(true)
        let reviewDict:[String: String] = ["reason": textForReview.text,"rank":"\(rate)"]
        sender.superview?.superview?.removeFromSuperview()
        let notificationName = Notification.Name("RideReview")
        NotificationCenter.default.post(name: notificationName, object: nil , userInfo: reviewDict)
    }
    
    // MARK: FloatRatingView Delegate Methods
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
        rate = rating
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        rate = rating
    }
    
    //Mark :- TextView Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3, animations:{
            self.superview?.frame = CGRect(x: 0, y: 60, width: UIScreen.main.bounds
                .width, height: 280)
        })
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3, animations:{
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            self.superview?.frame = CGRect(x: 0, y: height - 280, width: UIScreen.main.bounds.width, height: 280)
        })
    }
  
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            UIView.animate(withDuration: 0.3, animations:{
                self.superview?.endEditing(true)
                let bounds = UIScreen.main.bounds
                let height = bounds.size.height
                self.superview?.frame = CGRect(x: 0, y: height - 280, width: UIScreen.main.bounds.width, height: 280)
            })
            return true
        }
        return true
    }
}
