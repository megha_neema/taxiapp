//
//  CancellationView.swift
//  TaxiApp
//
//  Created by Megha on 5/17/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CancellationView: UIView {

    fileprivate var mainView: UIView!
    fileprivate let nibName = "CancellationView"
    
    @IBOutlet var reasonLblCollection: [UILabel]!
    @IBOutlet var reasonButtonArray: [UIButton]!
    
    var reasonString : String = ""
    
    // MARK: - view lifecycle
    public override init(frame: CGRect) {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        let width = bounds.size.width
        super.init(frame: CGRect(x: (width-300)/2 ,y: (height-311)/2, width: 300, height: 311))
        xibSetup()
    }
    
    open override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func xibSetup() {
        mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(mainView)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    @IBAction func cancelReasonAction(_ sender: UIButton) {
        
        for button : UIButton in reasonButtonArray {
            if button.isSelected == true {
                button.isSelected = false
            }
        }
        if sender.isSelected == true{
            sender.isSelected = false
        }else
        {
            sender.isSelected = true
            reasonString = ((reasonLblCollection as NSArray).object(at: sender.tag) as! UILabel).text!
        }
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        let reasonDict:[String: String] = ["reason": reasonString , "senderButton":"\(sender.tag)"]
        sender.superview?.superview?.superview?.removeFromSuperview()
        let notificationName = Notification.Name("RideCancel")
        NotificationCenter.default.post(name: notificationName, object: nil, userInfo: reasonDict)
    }
}
